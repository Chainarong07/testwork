package com.example.work;

import lombok.Data;

@Data
public class RequestDTO {
    private String username;
    private String password;
    private String nickName;
    private String email;
}
