package com.example.work;

import com.example.work.Utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response<T> {

    private Status status;
    private T payload;
    private Object errors;
    private Object metadata;

    public static <T> Response<T> badRequest() {
        Response<T> response = new Response<>();
        response.status = Status.BAD_REQUEST;
        return response;
    }

    public static <T> Response<T> ok() {
        Response<T> response = new Response<>();
        response.status = Status.OK;
        return response;
    }

    public static <T> Response<T> unauthorized() {
        Response<T> response = new Response<>();
        response.status = Status.UNAUTHORIZED;
        return response;
    }

    public static <T> Response<T> validationException() {
        Response<T> response = new Response<>();
        response.status = Status.VALIDATION_EXCEPTION;
        return response;
    }

    public static <T> Response<T> wrongCredentials() {
        Response<T> response = new Response<>();
        response.status = Status.WRONG_CREDENTIALS;
        return response;
    }

    public static <T> Response<T> accessDenied() {
        Response<T> response = new Response<>();
        response.status = Status.ACCESS_DENIED;
        return response;
    }

    public static <T> Response<T> exception() {
        Response<T> response = new Response<>();
        response.status = Status.EXCEPTION;
        return response;
    }

    public static <T> Response<T> notFound() {
        Response<T> response = new Response<>();
        response.status = Status.NOT_FOUND;
        return response;
    }

    public static <T> Response<T> duplicateEntity() {
        Response<T> response = new Response<>();
        response.status = Status.DUPLICATE_ENTITY;
        return response;
    }

    public void addErrorMsgToResponse(String errorMsg, Exception ex) {
        ResponseError error = new ResponseError().setDetails(errorMsg).setMessage(ex.getMessage())
                .setTimestamp(DateUtils.today());
        this.errors = error;
    }

    public enum Status {
        OK, BAD_REQUEST, UNAUTHORIZED, VALIDATION_EXCEPTION, EXCEPTION, WRONG_CREDENTIALS, ACCESS_DENIED, NOT_FOUND,
        DUPLICATE_ENTITY
    }

    @Getter
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PageMetadata {
        private int size;
        private long totalElements;
        private int totalPages;
        private int number;

        public PageMetadata(int size, long totalElements, int totalPages, int number) {
            this.size = size;
            this.totalElements = totalElements;
            this.totalPages = totalPages;
            this.number = number;
        }
    }

}
