package com.example.work;

import com.example.work.Entiry.User;
import com.example.work.Service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@CrossOrigin
@Log4j2
@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = {"/",""}, method = RequestMethod.GET)
    public List<User> index() {
        return userService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User index(@PathVariable int id){
        return userService.getUser(id);
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String index(@RequestBody RequestDTO requestDTO) {
        this.userService.addUser(requestDTO);
        return "Successfully";
    }
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public String updateUser(@PathVariable int id, @RequestBody RequestDTO requestDTO) {
        User user = userService.update(id,requestDTO);
        if (null == user) {
            return "Error";
        }
        return  "Successfully";
    }
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public String delUser(@PathVariable int id) {
        userService.delUser(id);
        return "Deleted Successfully";
    }


    }
