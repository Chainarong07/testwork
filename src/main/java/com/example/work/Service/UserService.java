package com.example.work.Service;

import com.example.work.Entiry.User;
import com.example.work.Repository.UserRepository;
import com.example.work.RequestDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public User getUser(int id) {
        return this.userRepository.findById(id).get();
    }

    public User addUser(RequestDTO requestDTO) {
        User user = new User();
        user.setUsername(requestDTO.getUsername());
        user.setPassword(requestDTO.getPassword());
        user.setNickName(requestDTO.getNickName());
        user.setEmail(requestDTO.getEmail());
        return this.userRepository.save(user);
    }

    public User update(int id, RequestDTO requestDTO) {
        User user = this.getUser(id);
        if (null != requestDTO.getNickName()) {
            user.setNickName(requestDTO.getNickName());
        }
        if (null != requestDTO.getEmail()) {
            user.setEmail(requestDTO.getEmail());
        }
        if (null != requestDTO.getPassword()) {
            user.setPassword(requestDTO.getPassword());
        }
        return userRepository.save(user);
    }

    public List<User> getAll() {
        return this.userRepository.findAll();
    }
    public void delUser(int id){
        userRepository.deleteById(id);
    }
}
